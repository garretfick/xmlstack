// Copyright 2012 Garret Fick
#ifndef XML_NULLMINIPARSER_H_
#define XML_NULLMINIPARSER_H_

#include <string>

#if defined XML_UNICODE
#define XMLCHAR wchar_t
#define XMLSTR std::wstring
#else
#define XMLCHAR char
#define XMLSTR std::string
#endif //XML_UNICODE

#if defined XML_HAS_OVERRIDE
#define OVERRIDE override
#else
#define OVERRIDE
#endif //XML_HAS_OVERRIDE

#if defined XML_HAS_FINAL
#define FINAL final
#else
#define FINAL
#endif //XML_HAS_FINAL

#endif
