// Copyright 2012 Garret Fick

#ifndef XML_XMLWRITERADAPTER_H_
#define XML_XMLWRITERADAPTER_H_

#pragma once

#include "basictypes.h"

namespace tinyxml2
{
	class XMLPrinter;
}

namespace xml
{

class XmlWriter;

class XmlElement
{
public:
	XmlElement(const char* const element_name, XmlWriter& xml_writer);
	virtual ~XmlElement();

	void		AddStringAttribute(const char* const attribute_name, const std::wstring& attribute_value);
	void		AddStringAttribute(const char* const attribute_name, const wchar_t* const attribute_value);
	void		AddBooleanAttribute(const char* const attribute_name, const bool attribute_value);
	void		AddDateTimeAttribute(const char* const attribute_name, const time_t attribute_value);
	void		AddDateAttribute(const char* const attribute_name, const time_t attribute_value);
	void		AddDoubleAttribute(const char* const attribute_name, const double attribute_value);

private:
	tinyxml2::XMLPrinter* xml_printer_;
};

class XmlWriter
{
	friend XmlElement;

public:
	XmlWriter();
	~XmlWriter();

	void Write(std::ostream& output_stream);

private:
	DISALLOW_COPY_AND_ASSIGN(XmlWriter)

protected:
	tinyxml2::XMLPrinter* xml_printer_;
};

} // namespace xml

#endif //XML_XMLWRITERADAPTER_H_