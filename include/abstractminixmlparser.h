// Copyright 2012 Garret Fick
#ifndef XML_ABSTRACTMINIXMLPARSER_H_
#define XML_ABSTRACTMINIXMLPARSER_H_

#pragma once

#include "xmlstack.h"
#include "minixmlparserinterface.h"

namespace xml
{

class AbstractMiniXmlParser : public MiniXmlParserInterface
{
public:
	AbstractMiniXmlParser() : this_depth_(0), child_parser_end_depth_(0), child_parser_(nullptr) {}
	virtual ~AbstractMiniXmlParser() {}

	virtual void StartElement	(const XMLSTR& name, const AttrList& attrs) OVERRIDE FINAL;
	virtual void EndElement		(const XMLSTR& name) OVERRIDE FINAL;
	virtual void CharacterData	(const XMLSTR& data) OVERRIDE;
	virtual void StartCData		() OVERRIDE;
	virtual void EndCData		() OVERRIDE;

protected:
	virtual bool StartElementImpl(const XMLSTR& name, const AttrList& attrs) = 0;
	virtual void EndElementImpl(const XMLSTR& name) = 0;

	size_t			GetDepth			() const { return this_depth_; }

	void			SetChildParser		(MiniXmlParserInterface* child_parser, const XMLSTR& name, const AttrList& attrs);

private:
	void			SetNullChildParser	();

private:
	size_t					this_depth_;

	size_t					child_parser_end_depth_;
	MiniXmlParserInterface*	child_parser_;
};

} // namespace xml

#endif //XML_ABSTRACTMINIXMLPARSER_H_