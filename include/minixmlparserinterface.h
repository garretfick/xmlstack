// Copyright 2012 Garret Fick
#ifndef XML_MINIXMLPARSERINTERFACE_H_
#define XML_MINIXMLPARSERINTERFACE_H_

#pragma once

#include "xmlstack.h"
#include "attrlist.h"

namespace xml
{

class AttrList;

class MiniXmlParserInterface
{
public:
	MiniXmlParserInterface() {}
	virtual ~MiniXmlParserInterface() {}

public:
	
	virtual void StartElement	(const XMLSTR& name, const AttrList& attrs) = 0;
	virtual void EndElement		(const XMLSTR& name) = 0;
	virtual void CharacterData	(const XMLSTR& data) = 0;
	virtual void StartCData		() = 0;
	virtual void EndCData		() = 0;
};

} // namespace xml

#endif //XML_MINIXMLPARSERINTERFACE_H_
