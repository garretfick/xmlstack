// Copyright 2012 Garret Fick
#ifndef XML_ATTRLIST_H_
#define XML_ATTRLIST_H_

#pragma once

#include "xmlstack.h"

namespace xml
{

class AttrList
{
public:

	AttrList() : attrs_(EMPTY_ATTRS) {}
	explicit AttrList(const XMLCHAR** attrs) : attrs_(attrs) {}

	void			Init		(const XMLCHAR** attrs) { (attrs == nullptr) ? attrs_ = attrs : EMPTY_ATTRS; }

	const XMLCHAR*	GetString	(const XMLCHAR* const name, const XMLCHAR* const defaultval) const;
	double			GetDouble	(const XMLCHAR* const name, const double defaultval) const;
	bool			GetBoolean	(const XMLCHAR* const name, const bool defaultval) const;
	int				GetInt		(const XMLCHAR* const name, const int defaultval) const;
	unsigned int	GetUInt		(const XMLCHAR* const name, const unsigned int defaultval) const;
	time_t			GetDate		(const XMLCHAR* const name, const time_t defaultval) const;

private:
	//The attr list is never nullptr so we don't need to check if it is null. It is either a valid
	//list or this special empty attrs, which is just an empty list
	static const XMLCHAR* EMPTY_ATTRS[];

	//The attributes
	const XMLCHAR**	attrs_;
};

} // namespace xml

#endif //XML_ATTRLIST_H_
