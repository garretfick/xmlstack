// Copyright 2012 Garret Fick
#ifndef XML_XMLPARSERADAPTER_H_
#define XML_XMLPARSERADAPTER_H_

#pragma once

struct XML_ParserStruct;

namespace xml
{

class MiniXmlParserInterface;

/// The CXmlParserAdapter forms the interface between the core xml parser and the
/// mini-parser architecture
class XmlParserAdapter
{
public:
	XmlParserAdapter();
	~XmlParserAdapter();

	errno_t		Parse(std::istream& input_stream, MiniXmlParserInterface& root_parser);

private:
	// Event handler functions (used to respond to the parser's events)
	static void __cdecl SaxElementStartHandler	(void *userdata, const XMLSTR *name, const XMLSTR **atts);
	static void __cdecl SaxElementEndHandler	(void *userdata, const XMLSTR *name);
	static void __cdecl SaxCharacterDataHandler	(void *userdata, const XMLSTR *s, int len);
	static void __cdecl SaxCDataStartHandler	(void *userdata);
	static void __cdecl SaxCDataEndHandler		(void *userdata);

private:
	XML_ParserStruct*			expat_parser_;			///< The core parser we are wrapping up
	MiniXmlParserInterface*		mini_xml_parser_;
	AttrList*					attr_list_;				///< A attr list wrapper so we don't have to allocate it repeatedly
};

} // namespace xml

#endif //XML_XMLPARSERADAPTER_H_
