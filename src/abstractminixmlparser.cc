// Copyright 2012 Garret Fick
#include "abstractminixmlparser.h"
#include "nullminixmlparser.h"

namespace xml
{

void AbstractMiniXmlParser::StartElement(const XMLSTR& name, const AttrList& attrs)
{
	// Increment the depth that we are tracking
	++this_depth_;

	// Either handle the event ourselves, or pass it on to the current child parser
	if (child_parser_ != nullptr)
	{
		child_parser_->StartElement(name, attrs);
	}
	else
	{
		bool handled = StartElementImpl(name, attrs);
		if (!handled && child_parser_ == nullptr)
		{
			SetNullChildParser();
		}
	}
}

void AbstractMiniXmlParser::EndElement(const XMLSTR& name)
{
	if (child_parser_ != nullptr)
	{
		if (this_depth_ == child_parser_end_depth_)
		{
			// This is the end of the child parser
			child_parser_->EndElement(name);
			delete child_parser_;
			child_parser_ = nullptr;
		}
		else
		{
			// Just pass the message along
			child_parser_->EndElement(name);
		}
	}
	else
	{
		EndElementImpl(name);
	}

	// Decrement the depth that we are tracking
	--this_depth_;
}

void AbstractMiniXmlParser::CharacterData(const XMLSTR& data)
{
	if (child_parser_ != nullptr)
	{
		child_parser_->CharacterData(data);
	}
}

void AbstractMiniXmlParser::StartCData()
{
	if (child_parser_ != nullptr)
	{
		child_parser_->StartCData();
	}
}

void AbstractMiniXmlParser::EndCData()
{
	if (child_parser_ != nullptr)
	{
		child_parser_->EndCData();
	}
}

/// Sets the child mini parser. This class will own the parser and delete it automatically
/// when the context for the mini parser ends
void AbstractMiniXmlParser::SetChildParser(MiniXmlParserInterface* child_parser, const XMLSTR& name, const AttrList& attrs)
{
	child_parser_end_depth_ = this_depth_;
	child_parser_ = child_parser;
	child_parser_->StartElement(name, attrs);
}

void AbstractMiniXmlParser::SetNullChildParser()
{
	child_parser_end_depth_ = this_depth_;
	child_parser_ = new xml::NullMiniXmlParser;
}
    
} // namespace xml

