// Copyright 2012 Garret Fick
#include <string>
#include "attrlist.h"
#include "minixmlparserinterface.h"
#include "xmlparseradapter.h"
#include "libexpat/expat.h"



libeda::core::parser::XmlParserAdapter::XmlParserAdapter()
	: mini_xml_parser_(nullptr)
	, attr_list_(new AttrList)
{
	expat_parser_ = XML_ParserCreate(nullptr);

	// The user data is passed to the callbacks so we can find "this"
	XML_SetUserData(expat_parser_, this);

	// Register to handle the events from the parser
	XML_SetElementHandler(expat_parser_, &XmlParserAdapter::SaxElementStartHandler, &XmlParserAdapter::SaxElementEndHandler);
	XML_SetCharacterDataHandler(expat_parser_, &XmlParserAdapter::SaxCharacterDataHandler);
	XML_SetCdataSectionHandler(expat_parser_, &XmlParserAdapter::SaxCDataStartHandler, &XmlParserAdapter::SaxCDataEndHandler);
}

libeda::core::parser::XmlParserAdapter::~XmlParserAdapter()
{
	XML_ParserFree(expat_parser_);
	delete attr_list_;
}

errno_t libeda::core::parser::XmlParserAdapter::Parse(std::istream& input_stream, MiniXmlParserInterface& root_parser)
{
	mini_xml_parser_ = &root_parser;

	XML_Status status(XML_STATUS_OK);
	std::string line;
	while (status == XML_STATUS_OK && input_stream.good())
	{
		std::getline(input_stream, line);
		status = XML_Parse(expat_parser_, line.c_str(), line.length(), XML_FALSE);
	}

	if (status == XML_STATUS_OK)
	{
		XML_Parse(expat_parser_, nullptr, 0, XML_TRUE);
	}

	mini_xml_parser_ = nullptr;

	return (status == XML_STATUS_OK) ? 0 : -1;
}

/// Handler for the start element event
void libeda::core::parser::XmlParserAdapter::SaxElementStartHandler(void *userdata, const wchar_t *name, const wchar_t **atts)
{
	XmlParserAdapter* me(static_cast<XmlParserAdapter*>(userdata));

	me->attr_list_->Init(atts);
	me->mini_xml_parser_->StartElement(std::wstring(name), *(me->attr_list_));
}

/// Handler for the end element event
void libeda::core::parser::XmlParserAdapter::SaxElementEndHandler(void *userdata, const wchar_t *name)
{
	XmlParserAdapter* me(static_cast<XmlParserAdapter*>(userdata));

	me->mini_xml_parser_->EndElement(std::wstring(name));
}

/// Handler for the character data event
void libeda::core::parser::XmlParserAdapter::SaxCharacterDataHandler(void *userdata, const wchar_t *s, int len)
{
	XmlParserAdapter* me(static_cast<XmlParserAdapter*>(userdata));

	me->mini_xml_parser_->CharacterData(std::wstring(s, len));
}

/// Handler for the start CData event
void libeda::core::parser::XmlParserAdapter::SaxCDataStartHandler(void *userdata)
{
	XmlParserAdapter* me(static_cast<XmlParserAdapter*>(userdata));

	me->mini_xml_parser_->StartCData();
}

/// Handler for the end CData event
void libeda::core::parser::XmlParserAdapter::SaxCDataEndHandler(void *userdata)
{
	XmlParserAdapter* me(static_cast<XmlParserAdapter*>(userdata));

	me->mini_xml_parser_->EndCData();
}
