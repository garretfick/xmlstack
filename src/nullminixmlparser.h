// Copyright 2012 Garret Fick
#ifndef XML_NULLMINIPARSER_H_
#define XML_NULLMINIPARSER_H_

#pragma once

#include "xmlstack.h"
#include "minixmlparserinterface.h"

namespace xml
{

/// Elements with the name name in different contexts can cause parsing
/// problems, particularly, where the context is unknown due to a nested
/// XML document. It prevents further childs elements from being wrongly
/// interpreted
/// Whenever you encounter an unknown element (or an element who's children
/// you won't handle, create an instance of this class, and set this as
/// the top-most mini-parser
class NullMiniXmlParser : public MiniXmlParserInterface
{
public:
	NullMiniXmlParser() {}
	virtual ~NullMiniXmlParser() {}

	virtual void	StartElement	(const XMLSTR& , const AttrList& ) OVERRIDE {}		///< Just consumes the message
	virtual void	EndElement		(const XMLSTR& ) OVERRIDE {}						///< Just consumes the message
	virtual void	CharacterData	(const XMLSTR& ) OVERRIDE {}						///< Just consumes the message
	virtual void	StartCData		() OVERRIDE {}										///< Just consumes the message
	virtual void	EndCData		() OVERRIDE {}										///< Just consumes the message
};

} // namespace xml

#endif //XML_NULLMINIPARSER_H_