// Copyright 2012 Garret Fick
#include  <cwchar>
#include "attrlist.h"

#if defined XML_UNICODE
#define XMLSTRCMP wcscmp
#define XMLTOD wcstod
#define XMLTOL wcstol
#define XMLTOUL wcstoul
#define XMLCHARID(x) L(x)
#else
#define XMLSTRCMP strcmp
#define XMLTOD strtod
#define XMLTOL strtol
#define XMLTOUL strtoul
#define XMLCHARID(x) (x)
#endif //XML_UNICODE

namespace xml
{

const XMLCHAR* AttrList::EMPTY_ATTRS[] = {nullptr, nullptr};

const XMLCHAR* AttrList::GetString(const XMLCHAR* const name, const XMLCHAR* const defaultval) const
{
	size_t attr_index(0);
	while(attrs_[attr_index] != nullptr)
	{
		if (XMLSTRCMP(name, attrs_[attr_index]) == 0)
		{
			return attrs_[attr_index + 1];
		}
		attr_index += 2;
	}
	return defaultval;
}

double AttrList::GetDouble(const XMLCHAR* const name, const double defaultval) const
{
	size_t attr_index(0);
	while(attrs_[attr_index] != nullptr)
	{
		if (XMLSTRCMP(name, attrs_[attr_index]) == 0)
		{
			return XMLTOD(attrs_[attr_index + 1], nullptr);
		}
		attr_index += 2;
	}
	return defaultval;
}

bool AttrList::GetBoolean(const XMLCHAR* const name, const bool defaultval) const
{
	size_t attr_index(0);
	while(attrs_[attr_index] != nullptr)
	{
		if (XMLSTRCMP(name, attrs_[attr_index]) == 0)
		{
			return (XMLSTRCMP(attrs_[attr_index + 1], XMLCHARID("1")) == 0 || XMLSTRCMP(attrs_[attr_index + 1], XMLCHARID("true")) == 0);
		}
		attr_index += 2;
	}
	return defaultval;
}

int AttrList::GetInt(const XMLCHAR* const name, const int defaultval) const
{
	size_t attr_index(0);
	while(attrs_[attr_index] != nullptr)
	{
		if (XMLSTRCMP(name, attrs_[attr_index]) == 0)
		{
			return static_cast<int>(XMLTOL(attrs_[attr_index + 1], nullptr, 10));
		}
		attr_index += 2;
	}
	return defaultval;
}

unsigned int AttrList::GetUInt(const XMLCHAR* const name, const unsigned int defaultval) const
{
	size_t attr_index(0);
	while(attrs_[attr_index] != nullptr)
	{
		if (XMLSTRCMP(name, attrs_[attr_index]) == 0)
		{
			return static_cast<int>(XMLTOUL(attrs_[attr_index + 1], nullptr, 10));
		}
		attr_index += 2;
	}
	return defaultval;
}

time_t AttrList::GetDate(const XMLCHAR* const name, const time_t defaultval) const
{
	size_t attr_index(0);
	while(attrs_[attr_index] != nullptr)
	{
		if (XMLSTRCMP(name, attrs_[attr_index]) == 0)
		{
			//Fill in the tm struct with the various values from the date
			//Make sure tm_isdst is -1 to avoid daylight time problems

			//Initialize to the special 0 value
			tm datetime;
			datetime.tm_sec = 0;
			datetime.tm_min = 0;
			datetime.tm_hour = 0;
			datetime.tm_mday = 1;
			datetime.tm_mon = 0;
			datetime.tm_year = -1899;
			datetime.tm_wday = 0;
			datetime.tm_yday = 0;
			datetime.tm_isdst = -1;

			wchar_t tzdir('+');
			
			//Don't parse if this is the special value
			bool valid(false);
			if (XMLSTRCMP(attrs_[attr_index + 1], XMLCHARID("0")) != 0)
			{
				//@TODO need to parse proper with time zones
				//'-'? yyyy '-' mm '-' dd zzzzzz?
				//2002-10-10+13:00
				//2002-10-10
				
				int result = swscanf(attrs_[attr_index + 1], L"%4d-%2d-%2d%1c%2d:%2d", &datetime.tm_year, &datetime.tm_mon, &datetime.tm_mday, &tzdir, &datetime.tm_hour, &datetime.tm_min);
				datetime.tm_year = datetime.tm_year - 1900;
				if (result == 3 || result == 4 || (result > 4 && tzdir != L'-' && tzdir != L'+'))
				{
					
					datetime.tm_min = 0;
					datetime.tm_hour = 0;
					valid = true;
				}
				else if (result == 5)
				{
					datetime.tm_min = 0;
					valid = true;
				}
				else if (result == 6)
				{
					valid = true;
				}
			}
			else
			{
				valid = true;
			}
			
			if (valid)
			{
				time_t utc_time =  mktime(&datetime);
				return utc_time;
			}
			else
			{
				return defaultval;
			}
		}
		attr_index += 2;
	}
	return defaultval;
}
    
} // namespace xml

