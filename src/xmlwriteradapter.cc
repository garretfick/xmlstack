// Copyright 2012 Garret Fick
#include "xmlwriteradapter.h"
#include "third_party/tinyxml2/tinyxml2.h"

libeda::core::writer::XmlElement::XmlElement(const char* const element_name, XmlWriter& xml_writer)
	: xml_printer_(xml_writer.xml_printer_)
{
	xml_printer_->OpenElement(element_name);
}

libeda::core::writer::XmlElement::~XmlElement()
{
	xml_printer_->CloseElement();
}

void libeda::core::writer::XmlElement::AddStringAttribute(const char* const attribute_name, const wchar_t* const attribute_value)
{
	size_t nRequiredSize = wcstombs(nullptr, attribute_value, 0);
	char* buffer(new char[nRequiredSize + 1]);
	wcstombs(buffer, attribute_value, nRequiredSize + 1);
	xml_printer_->PushAttribute(attribute_name, buffer);
	delete [] buffer;
}

void libeda::core::writer::XmlElement::AddStringAttribute(const char* const attribute_name, const std::wstring& attribute_value)
{
	AddStringAttribute(attribute_name, attribute_value.c_str());
}

void libeda::core::writer::XmlElement::AddBooleanAttribute(const char* const attribute_name, const bool attribute_value)
{
	xml_printer_->PushAttribute(attribute_name, attribute_value);
}

void libeda::core::writer::XmlElement::AddDateTimeAttribute(const char* const attribute_name, const time_t attribute_value)
{
	
}

void libeda::core::writer::XmlElement::AddDateAttribute(const char* const attribute_name, const time_t attribute_value)
{
	
}

void libeda::core::writer::XmlElement::AddDoubleAttribute(const char* const attribute_name, const double attribute_value)
{
	xml_printer_->PushAttribute(attribute_name, attribute_value);
}

libeda::core::writer::XmlWriter::XmlWriter()
	: xml_printer_(nullptr)
{
	xml_printer_ = new tinyxml2::XMLPrinter;
}

libeda::core::writer::XmlWriter::~XmlWriter()
{
	delete xml_printer_;
}

void libeda::core::writer::XmlWriter::Write(std::ostream& output_stream)
{
	output_stream << xml_printer_->CStr();
}
